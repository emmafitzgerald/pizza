#!/usr/bin/python

import math
import numpy as np
import scipy.stats
import matplotlib
matplotlib.use("Agg")
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt 
import os

colours = ["blue", "red", "yellow", "purple", "orange", "green", "black", "cyan"]

# Get average and confidence interval delta of a vector of values
def process_vector(v):
    if len(v) == 0:
        print "Empty vector"
        return (0, 0) 
    avg = np.mean(v)
    stddev = np.std(v)
    n = len(v)
    delta = 1.96 * (stddev / math.sqrt(n))
    return (avg, delta)

def scatter_obj_SINR(data, filename, metric="min", objective="min_angle"):
    fig, ax = plt.subplots()

    if objective == "opt_obj":
        obj = data[:,2]
    elif objective == "min_angle":
        obj = data[:,3]
    if metric == "min":
        SINR = data[:,4]
    elif metric == "avg_min":
        SINR = data[:, 5]
    elif metric == "avg":
        SINR = data[:, 6]
    r = scipy.stats.pearsonr(obj, SINR)
    print "Correlation between objective and SINR metric for metric", metric, "and objective", objective, r

    i = 0
    #for num_nodes in [15, 20, 25, 30, 35, 40]:
    for num_nodes in [15, 18, 21, 24, 27, 30, 33, 36]:
        idx = data[:,0] == num_nodes
        sel_data = data[idx]

        if objective == "opt_obj":
            obj = sel_data[:,2]
        elif objective == "min_angle":
            obj = sel_data[:,3]

        if metric == "min":
            SINR = sel_data[:,4]
        elif metric == "avg_min":
            SINR = sel_data[:, 5]
        elif metric == "avg":
            SINR = sel_data[:, 6]
        plt.scatter(obj, SINR, color=colours[i])
        
        r = scipy.stats.pearsonr(obj, SINR)
        print "Correlation for", num_nodes, "nodes:", r

        i += 1

    if objective == "min_angle":
        plt.xlabel("Minimum angle")
    elif objective == "opt_obj":
        plt.xlabel("Objective value")
    plt.ylabel("SINR")

    plt.savefig(filename)
    plt.cla()

def bar_SINR(data, data_labels, filename, metric="min"):
    fig, ax = plt.subplots()

    vals = []
    err = []

    for dataset in data:
        if metric == "min":
            SINR = dataset[:,4]
        elif metric == "avg_min":
            SINR = dataset[:, 5]
        elif metric == "avg":
            SINR = dataset[:, 6]
        
        avg, delta = process_vector(SINR)
        vals.append(avg)
        err.append(delta)

    x = range(1, len(data) + 1)
    plt.bar(x, vals, yerr=err)

    plt.xticks(x, data_names)
    plt.xlabel("Algorithm")
    plt.ylabel("SINR")

    plt.savefig(filename)
    plt.cla()

def bar_num_nodes_SINR(data, data_labels, filename, metric="min", logy=False):
    fig, ax = plt.subplots()

    bars = []
    #x = [15, 20, 25, 30, 35, 40]
    x = [15, 18, 21, 24, 27, 30, 33, 36]

    width = 2.8
    step = float(width)/float(len(data))
    ind = np.array(x)

    i = 0
    for dataset in data:
        vals = []
        err = []

        for num_nodes in x:
            idx = dataset[:,0] == num_nodes
            sel_data = dataset[idx]
            if metric == "min":
                SINR = sel_data[:,4]
            elif metric == "avg_min":
                SINR = sel_data[:, 5]
            elif metric == "avg":
                SINR = sel_data[:, 6]
            elif metric == "max":
                SINR = sel_data[:, 8]
            
            avg, delta = process_vector(SINR)
            vals.append(avg)
            err.append(delta)

        label = data_names[i]
        pos = i*step - width/2.0 
        bar = plt.bar(ind + pos, vals, step, yerr=err, label=label)
        bars.append(bar)

        i += 1

    if logy:
        plt.yscale("log")

    plt.xticks(x)
    plt.legend()
    plt.xlabel("Number of nodes")
    plt.ylabel("SINR")

    plt.savefig(filename)
    plt.cla()

def bar_obj(opt_data, bounce_data, filename):
    fig, ax = plt.subplots()

    bars = []
    #x = [15, 20, 25, 30, 35, 40]
    x = [15, 18, 21, 24, 27, 30, 33, 36]
    data_names = ["Optimal", "Approximation"]

    width = 2.8
    step = float(width)/2.0
    ind = np.array(x)

    i = 0
    first = True
    for dataset in [opt_data, bounce_data]:
        vals = []
        err = []

        for num_nodes in x:
            idx = dataset[:,0] == num_nodes
            sel_data = dataset[idx]
            
            obj = sel_data[:,2]
            
            avg, delta = process_vector(obj)
            vals.append(math.degrees(avg))
            err.append(math.degrees(delta))

        label = data_names[i]
        pos = i*step - width/2.0 
        bar = plt.bar(ind + pos, vals, step, yerr=err, label=label)
        bars.append(bar)
        print vals
        if first:
            opt_vals = vals
            first = False
        else:
            bounce_vals = vals

        i += 1

    i = 0
    ratios = []
    for val in opt_vals:
        ratios.append(bounce_vals[i] / val)
        i += 1

    avg, delta = process_vector(ratios)
    print "Approximation achieves", avg, " (+/-", delta, ") compared to optimal"

    plt.xticks(x)
    plt.legend(loc="lower left")
    plt.xlabel("Number of nodes")
    plt.ylabel("Objective (degrees)")

    plt.savefig(filename)
    plt.cla()

opt_data = np.genfromtxt("optimal_results.dat")
scatter_obj_SINR(opt_data, "figures/scatter_opt_obj_min_SINR_objective.eps", metric="min", objective="opt_obj")
scatter_obj_SINR(opt_data, "figures/scatter_opt_obj_min_SINR.eps", metric="min")
scatter_obj_SINR(opt_data, "figures/scatter_opt_obj_avg_min_SINR.eps", metric="avg_min")
scatter_obj_SINR(opt_data, "figures/scatter_opt_obj_avg_SINR.eps", metric="avg")

ord_data = np.genfromtxt("ordered_results.dat")
scatter_obj_SINR(ord_data, "figures/scatter_ord_obj_min_SINR.eps", metric="min")
scatter_obj_SINR(ord_data, "figures/scatter_ord_obj_avg_min_SINR.eps", metric="avg_min")
scatter_obj_SINR(ord_data, "figures/scatter_ord_obj_avg_SINR.eps", metric="avg")

rand_data = np.genfromtxt("random_results.dat")
scatter_obj_SINR(rand_data, "figures/scatter_rand_obj_min_SINR.eps", metric="min")
scatter_obj_SINR(rand_data, "figures/scatter_rand_obj_avg_min_SINR.eps", metric="avg_min")
scatter_obj_SINR(rand_data, "figures/scatter_rand_obj_avg_SINR.eps", metric="avg")

clumped_data = np.genfromtxt("clumped_results.dat")
scatter_obj_SINR(clumped_data, "figures/scatter_clumped_obj_min_SINR.eps", metric="min")
scatter_obj_SINR(clumped_data, "figures/scatter_clumped_obj_avg_min_SINR.eps", metric="avg_min")
scatter_obj_SINR(clumped_data, "figures/scatter_clumped_obj_avg_SINR.eps", metric="avg")

bounce_data = np.genfromtxt("bounce_results.dat")
scatter_obj_SINR(bounce_data, "figures/scatter_bounce_obj_min_SINR_objective.eps", metric="min", objective="opt_obj")
scatter_obj_SINR(bounce_data, "figures/scatter_bounce_obj_min_SINR.eps", metric="min")
scatter_obj_SINR(bounce_data, "figures/scatter_bounce_obj_avg_min_SINR.eps", metric="avg_min")
scatter_obj_SINR(bounce_data, "figures/scatter_bounce_obj_avg_SINR.eps", metric="avg")

power_data = np.genfromtxt("power_results.dat")
scatter_obj_SINR(power_data, "figures/scatter_power_obj_min_SINR.eps", metric="min")
scatter_obj_SINR(power_data, "figures/scatter_power_obj_avg_min_SINR.eps", metric="avg_min")
scatter_obj_SINR(power_data, "figures/scatter_power_obj_avg_SINR.eps", metric="avg")

data = [opt_data, bounce_data, clumped_data, power_data]
data_names = ["Optimal", "Approximation", "Clumped", "Power"]
bar_SINR(data, data_names, "figures/bar_min_SINR.eps", metric="min")
bar_SINR(data, data_names, "figures/bar_avg_SINR.eps", metric="avg")
bar_num_nodes_SINR(data, data_names, "figures/bar_num_nodes_SINR_min.eps", metric="min")
bar_num_nodes_SINR(data, data_names, "figures/bar_num_nodes_SINR_avg.eps", metric="avg")
bar_num_nodes_SINR(data, data_names, "figures/bar_num_nodes_SINR_avg_min.eps", metric="avg_min")
bar_num_nodes_SINR(data, data_names, "figures/bar_num_nodes_SINR_max.eps", metric="max")

bar_obj(opt_data, bounce_data, "figures/bar_obj.eps")

os.chdir("./figures")
os.system("for i in ./*.eps; do epstopdf $i; done")
os.chdir("..")
