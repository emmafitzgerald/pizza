#!/usr/bin/python

import math
import cmath
import numpy as np
import scipy.stats
import random

class Node:

    def __init__(self, ID, dom_dir, spec_spread=0.0):
        self.ID = ID
        self.dom_dir = dom_dir
        self.spec_spread = spec_spread
        self.angular_shift = math.pi * spec_spread
        self.min_angle = self.dom_dir - self.angular_shift
        self.max_angle = self.dom_dir + self.angular_shift

        self.channel = []
        self.group = -1

    def set_spec_spread(self, spec_spread):
        self.spec_spread = spec_spread
        self.angular_shift = math.pi * spec_spread
        self.min_angle = self.dom_dir - self.angular_shift
        self.max_angle = self.dom_dir + self.angular_shift

    def assign_group(self, group):
        self.group = group

    def set_channel(self, channel):
        self.channel = channel
        self.power = sum([abs(z)**2.0 for z in channel])

class SINRMetric:

    def __init__(self, min_sinr, avg_min, avg_sinr, avg_max, max_sinr, avg_stddev):
        self.min_sinr = min_sinr
        self.avg_min = avg_min
        self.avg_sinr = avg_sinr
        self.avg_max = avg_max
        self.max_sinr = max_sinr
        self.avg_stddev = avg_stddev

    def to_list(self):
        return [self.min_sinr, self.avg_min, self.avg_sinr, self.avg_max, self.max_sinr, self.avg_stddev]

scenario_dir = "scenarios/"
opt_solutions_dir = "solutions/"
num_pilots = 12

def random_groups(nodes, num_groups=3):
    for node in nodes:
        node.assign_group(random.choice(xrange(num_groups)))

def read_pizza_data(path):
    nodes = []

    infile = open(path, "r")

    state = "START"
    for line in infile:
        if state == "START":
            if line.startswith("param: dominant_direction"):
                state = "DOM_DIR"
            elif line.startswith("param: spectral_spread"):
                state = "SPEC_SPREAD"
            continue
        if state == "DOM_DIR":
            if line.startswith(";"):
                state = "START"
                continue

            ID, dom_dir = line.split()
            node = Node(int(ID), float(dom_dir))
            nodes.append(node)
            continue
        if state == "SPEC_SPREAD":
            if line.startswith(";"):
                state = "START"
                continue

            ID, spec_spread = line.split()
            node = nodes[int(ID) - 1]
            node.set_spec_spread(float(spec_spread))

    infile.close()

    return nodes

def read_channel_data(path, nodes):
    infile = open(path, "r")

    i = 0
    for line in infile:
        pieces = line.split()
        channel = []
        
        for piece in pieces:
            c = complex(piece)
            channel.append(c)
        
        nodes[i].set_channel(channel)
        i += 1

    infile.close()

def read_optimisation_data(path):
    infile = open(path, "r")

    obj = -1
    groups = {}

    state = "START"

    for line in infile:
        if state == "START":
            if "objective" in line:
                pieces = line.split()
                obj = float(pieces[-1])
                continue
            elif line.startswith("assignment"):
                state = "GROUPS"
                continue
        if state == "GROUPS":
            if line.startswith(";"):
                state = "START"
                continue

            pieces = line.split()
            ID = int(pieces[0])
            group = int(pieces[1])
            groups[ID] = group
            continue

    infile.close()

    return obj, groups

def optimal_groups(nodes, groups):
    for ID in groups.keys():
        nodes[ID - 1].assign_group(groups[ID])

def SINR(nodes, ID, precoding="MRC", num_nodes=6):
    rho = 1.0         # transmission power
    sigma_sq = 0.01    # noise

    # Fair power control
    min_ampl = 10000000000000000000000.0
    for node in nodes:
        ampl = sum(abs(z) for z in node.channel)
        if ampl < min_ampl:
            min_ampl = ampl

    # Find the node we are computing the SINR for
    i = 0
    channels = []
    for node in nodes:
        if node.ID == ID:
            self_node = node
            self_node_index = i
            #print "Channel amp:", sum(abs(x) for x in self_node.channel)

        # Fair power control
        #ampl = sum(abs(z) for z in node.channel)
        #scale = min_ampl / ampl
        #chan = [z * scale for z in node.channel]
        chan = node.channel
        
        channels.append(np.array(chan))

        i += 1

    channel_matrix = np.stack(channels)

    if precoding == "MRC":
        # H^H
        precoding_matrix = channel_matrix.conj().T
    elif precoding == "ZF":
        # H^H * (H*H^H)^(-1)
        inner = np.matmul(channel_matrix, channel_matrix.conj().T)
        right = np.linalg.inv(inner)
        left = channel_matrix.conj().T
        precoding_matrix = np.matmul(left, right)
    elif precoding == "RZF":
        # H^H * (H*H^H + aI)^(-1)
        inner1 = np.matmul(channel_matrix, channel_matrix.conj().T)
        coeff = float(num_nodes) / (rho / sigma_sq)
        inner2 = coeff * np.identity(num_nodes)
        right = np.linalg.inv(inner1 + inner2)
        left = channel_matrix.conj().T
        precoding_matrix = np.matmul(left, right)
    elif precoding == "MMSE":
        SNR = float(rho) / float(sigma_sq)
        reg = num_nodes / SNR
        right = reg * np.identity(num_nodes)
        chanT = channel_matrix.T
        chan_herm = chanT.conj().T
        mat_sum = np.matmul(chan_herm, chanT) + right
        W = np.matmul(np.linalg.inv(mat_sum), chan_herm)
        W_fro = np.linalg.norm(W, ord="fro")
        BS_antennas = np.shape(chanT)[0]
        n = (W_fro * W_fro) / BS_antennas
        precoding_matrix = W * (1.0 / math.sqrt(n))
        precoding_matrix = precoding_matrix.T

    chan = np.array([channel_matrix[self_node_index]])
    chan_precode = np.array([precoding_matrix[:,self_node_index]]).T
    signal = np.matmul(chan, chan_precode) 
    signal = abs(signal) * abs(signal)
    #print "Signal:", signal

    interference = 0.0
    i = 0
    for node in nodes:
        if node.ID == ID:
            i += 1
            continue
        chan2_precode = np.array([precoding_matrix[:,i]]).T
        val = np.matmul(chan, chan2_precode)
        val = abs(val) * abs(val)
        interference += val
        i += 1
    #print "Interference:", interference

    SINR = (rho * signal) / (sigma_sq + rho * interference)
    #print "SINR:", SINR
    #raw_input()

    return float(SINR), signal

def SINR_metrics(nodes, precoding="MRC"):
    groups = {}

    for node in nodes:
        if node.group not in groups.keys():
            groups[node.group] = []
        groups[node.group].append(node)

    sinr_metrics = {}
    group_avg = 0.0
    group_min = 0.0
    group_max = 0.0
    avg_min = 0.0
    avg_max = 0.0
    avg_stddev = 0.0
    first = True

    avg_signal_strength = 0
    for group in groups.keys():
        group_nodes = groups[group]
        test_sinr, test_sig = SINR(group_nodes, group_nodes[0].ID, precoding=precoding, num_nodes=len(group_nodes))
        max_sinr = test_sinr
        min_sinr = test_sinr
        avg_sinr = 0.0

        node_sinrs = []
        for node in group_nodes:
            node_sinr, signal = SINR(group_nodes, node.ID, precoding=precoding, num_nodes=len(group_nodes))
            avg_sinr += node_sinr
            avg_signal_strength += signal
            if max_sinr < node_sinr:
                max_sinr = node_sinr
            if min_sinr > node_sinr:
                min_sinr = node_sinr

            node_sinrs.append(node_sinr)

        avg_sinr /= float(len(group_nodes))
        stddev = np.std(node_sinrs)

        group_avg += avg_sinr
        if first:
            group_min = min_sinr
            group_max = max_sinr
            first = False
        avg_min += min_sinr
        avg_max += max_sinr
        if min_sinr < group_min:
            group_min = min_sinr
        if max_sinr > group_max:
            group_max = max_sinr
        avg_stddev += stddev

    group_avg /= float(len(groups.keys()))
    avg_min /= float(len(groups.keys()))
    avg_max /= float(len(groups.keys()))
    avg_stddev /= float(len(groups.keys()))
    avg_signal_strength /= float(len(nodes))

    print "Mean signal strength:", avg_signal_strength
    #raw_input()

    sinr_metrics = SINRMetric(group_min, avg_min, group_avg, avg_max, group_max, avg_stddev)
    return sinr_metrics

def channel_correlation(nodes):
    groups = {}

    for node in nodes:
        if node.group not in groups.keys():
            groups[node.group] = []
        groups[node.group].append(node)

    avg_corr = 0.0
    for group in groups:
        group_nodes = groups[group]
        corr = []
        for n1 in group_nodes:
            for n2 in group_nodes:
                if n1.ID == n2.ID:
                    continue

                c1 = n1.channel
                c2 = n2.channel
                # TODO: fix this, numpy does something strange with complex numbers here
                r = np.corrcoef(c1, c2)
                corr.append(r)
        avg_corr += np.linalg.norm(corr)
        #avg_corr += np.sum(corr)
    avg_corr /= float(len(groups.keys()))

    return avg_corr 

def get_dom_dir(node):
    return node.dom_dir

def ordered_groups(nodes, num_groups=3):
    sorted_nodes = sorted(nodes, key=get_dom_dir)

    group = 1
    for node in sorted_nodes:
        node.assign_group(group)
        group += 1
        if group > num_groups:
            group = 1

def clumped_groups(nodes, num_groups=3):
    sorted_nodes = sorted(nodes, key=get_dom_dir)
    
    nodes_per_group = len(nodes) / num_groups 

    group = 1
    nodes_in_group = 0
    for node in sorted_nodes:
        node.assign_group(group)
        nodes_in_group += 1
        if nodes_in_group >= nodes_per_group and group < num_groups:
            group += 1
            nodes_in_group = 0

def compute_min_angle(nodes):
    groups = {}

    for node in nodes:
        if node.group not in groups.keys():
            groups[node.group] = []
        groups[node.group].append(node)

    min_angle = 2.0*math.pi
    for group in groups:
        group_nodes = groups[group]
        for n1 in group_nodes:
            for n2 in group_nodes:
                if n1.ID == n2.ID:
                    continue
                angle = math.fabs(n1.dom_dir - n2.dom_dir) 
                if angle < min_angle:
                    min_angle = angle 

    return angle

def compute_objective(nodes):
    groups = {}

    for node in nodes:
        if node.group not in groups.keys():
            groups[node.group] = []
        groups[node.group].append(node)

    min_angle = 2.0*math.pi
    obj = 4.0*math.pi
    for group in groups:
        group_nodes = groups[group]
        sorted_nodes = sorted(group_nodes, key=get_dom_dir)
        i = 0
        for n1 in sorted_nodes:
            if i+1 < len(sorted_nodes):
                n2 = sorted_nodes[i+1]
            else:
                n2 = sorted_nodes[0]

            if i+1 < len(sorted_nodes):
                shifted_diff = math.fabs(n2.max_angle - n1.min_angle)
                angle_diff = math.fabs(n2.dom_dir - n1.dom_dir)
            else:
                shifted_diff = math.fabs(n2.max_angle + 2.0*math.pi - n1.min_angle)
                angle_diff = math.fabs(n2.dom_dir + 2.0*math.pi - n1.dom_dir)
            if shifted_diff > math.pi:
                shifted_diff = math.pi


            if shifted_diff < obj:
                obj = shifted_diff
            if angle_diff < min_angle:
                min_angle = angle_diff
            i += 1

    return min_angle, obj

def get_min_angle(node):
    return node.min_angle

def get_max_angle(node):
    return node.min_angle

def bouncy_groups(nodes, num_groups=3):
    sorted_nodes_min = sorted(nodes, key=get_min_angle) 

    first_node = sorted_nodes_min.pop(0)
    first_node.assign_group(1)

    sorted_nodes_max = sorted(sorted_nodes_min, key=get_max_angle)
    group = 2

    for node in sorted_nodes_max:
        node.assign_group(group)
        group += 1
        if group > num_groups:
            group = 1

def get_power(node):
    return node.power

def power_groups(nodes, num_groups=3):
    sorted_nodes = sorted(nodes, key=get_power)
    group = 1
    nodes_in_group = 0

    nodes_per_group = len(nodes) / num_groups

    for node in sorted_nodes:
        node.assign_group(group)
        nodes_in_group += 1
        if nodes_in_group >= nodes_per_group and group < num_groups:
            group += 1
            nodes_in_group = 0

def print_groups(nodes):
    groups = {}
    for node in nodes:
        if node.group not in groups.keys():
            groups[node.group] = []
        groups[node.group].append(node)

    for group in groups.keys():
        print "Group", group
        group_nodes = sorted(groups[group], key=get_dom_dir)
        for node in group_nodes:
            print "Node", node.ID, "dom dir:", node.dom_dir, "spectral spread:", node.spec_spread

opt_out = open("optimal_results.dat", "w")
opt_out.write("# Num nodes\trun\tobjective\tmin angle\tmin SINR\tavg min SINR\tavg SINR\tavg max SINR\tmax SINR\tSINR stddev\n")
ord_out = open("ordered_results.dat", "w")
ord_out.write("# Num nodes\trun\tobjective\tmin angle\tmin SINR\tavg min SINR\tavg SINR\tavg max SINR\tmax SINR\tSINR stddev\n")
rand_out = open("random_results.dat", "w")
rand_out.write("# Num nodes\trun\tobjective\tmin angle\tmin SINR\tavg min SINR\tavg SINR\tavg max SINR\tmax SINR\tSINR stddev\n")
clumped_out = open("clumped_results.dat", "w")
clumped_out.write("# Num nodes\trun\tobjective\tmin angle\tmin SINR\tavg min SINR\tavg SINR\tavg max SINR\tmax SINR\tSINR stddev\n")
bounce_out = open("bounce_results.dat", "w")
bounce_out.write("# Num nodes\trun\tobjective\tmin angle\tmin SINR\tavg min SINR\tavg SINR\tavg max SINR\tmax SINR\tSINR stddev\n")
power_out = open("power_results.dat", "w")
power_out.write("# Num nodes\trun\tobjective\tmin angle\tmin SINR\tavg min SINR\tavg SINR\tavg max SINR\tmax SINR\tSINR stddev\n")

#for num_nodes in [15, 20, 25, 30, 35, 40]:
for num_nodes in [15, 18, 21, 24, 27, 30, 33, 36]:
    cases = 0
    diff = []
    for run in xrange(1, 21):
        ppath = scenario_dir + str(num_nodes) + "_nodes_" + str(run) + "_pizza_data.dat"
        cpath = scenario_dir + str(num_nodes) + "_nodes_" + str(run) + "_channel_data.dat"
        
        nodes = read_pizza_data(ppath)

        avg_spec_spread = 0.0
        for node in nodes:
            avg_spec_spread += node.spec_spread
        avg_spec_spread /= float(len(nodes))
        print avg_spec_spread

        read_channel_data(cpath, nodes)

        opath = opt_solutions_dir + "solution_" + str(num_nodes) + "nodes_run" + str(run) + "_.out"

        try:
            obj, groups = read_optimisation_data(opath)
        except:
            print "Error, skipping"
            print "Number of nodes:", num_nodes
            print "Run:", run
            continue

        if obj == -1:
            # No solution was found for the optimisation problem
            continue

        #num_groups = int(math.ceil(num_nodes / float(num_pilots)))
        num_groups = 3

        precoding = "MRC"
        
        all_metrics = SINR_metrics(nodes, precoding=precoding)

        print "optimal"
        optimal_groups(nodes, groups)
        print_groups(nodes)
        opt_metrics = SINR_metrics(nodes, precoding=precoding)
        opt_obj_dd, opt_obj = compute_objective(nodes)
        opt_corr = channel_correlation(nodes)

        ordered_groups(nodes, num_groups=num_groups)
        ord_metrics = SINR_metrics(nodes, precoding=precoding)
        ord_obj_dd, ord_obj = compute_objective(nodes)
        ord_corr = channel_correlation(nodes)

        random_groups(nodes, num_groups=num_groups)
        rand_metrics = SINR_metrics(nodes, precoding=precoding)
        rand_obj_dd, rand_obj = compute_objective(nodes)
        rand_corr = channel_correlation(nodes)

        clumped_groups(nodes, num_groups=num_groups)
        clumped_metrics = SINR_metrics(nodes, precoding=precoding)
        clumped_obj_dd, clumped_obj = compute_objective(nodes)
        clumped_corr = channel_correlation(nodes)

        print "bouncy"
        bouncy_groups(nodes, num_groups=num_groups)
        print_groups(nodes)
        bounce_metrics = SINR_metrics(nodes, precoding=precoding)
        bounce_obj_dd, bounce_obj = compute_objective(nodes)
        bounce_corr = channel_correlation(nodes)
        if bounce_metrics.min_sinr > opt_metrics.min_sinr:
            cases += 1
        #if bounce_metrics.min_sinr > 3:
        #    raw_input()
        diff.append(opt_metrics.min_sinr - bounce_metrics.min_sinr)

        power_groups(nodes, num_groups=num_groups)
        power_metrics = SINR_metrics(nodes, precoding=precoding)
        power_obj_dd, power_obj = compute_objective(nodes)
        power_corr = channel_correlation(nodes)

        print "Objective for optimisation:", obj
        print "SINR metrics for all nodes:", all_metrics.to_list()

        print "Min angle for optimisation", opt_obj_dd
        print "SINR metrics for optimisation:", opt_metrics.to_list()
        print "Channel correlation for optimisation", opt_corr

        print "Min angle for ordered groups", ord_obj_dd
        print "SINR metrics for ordered groups:", ord_metrics.to_list()
        print "Channel correlation for ordered groups", ord_corr

        print "Min angle for random groups", rand_obj_dd
        print "SINR metrics for random groups:", rand_metrics.to_list()
        print "Channel correlation for random groups", rand_corr

        print "Min angle for clumped groups", clumped_obj_dd
        print "SINR metrics for clumped groups:", clumped_metrics.to_list()
        print "Channel correlation for clumped groups", clumped_corr

        print "Min angle for bouncy groups", bounce_obj_dd
        print "Objective for bouncy groups", bounce_obj
        print "SINR metrics for bouncy groups:", bounce_metrics.to_list()
        print "Channel correlation for bouncy groups", bounce_corr

        print "Min angle for amplitude groups", power_obj_dd
        print "SINR metrics for amplitude groups:", power_metrics.to_list()
        print "Channel correlation for amplitude groups", power_corr

        opt_out.write(str(num_nodes) + "\t" + str(run) + "\t" + str(obj) + "\t" + str(opt_obj_dd))
        for metric in opt_metrics.to_list():
            opt_out.write("\t" + str(metric))
        opt_out.write("\n")

        ord_out.write(str(num_nodes) + "\t" + str(run) + "\t" + str(ord_obj) + "\t" + str(ord_obj_dd))
        for metric in ord_metrics.to_list():
            ord_out.write("\t" + str(metric))
        ord_out.write("\n")

        rand_out.write(str(num_nodes) + "\t" + str(run) + "\t" + str(rand_obj) + "\t" + str(rand_obj_dd))
        for metric in rand_metrics.to_list():
            rand_out.write("\t" + str(metric))
        rand_out.write("\n")

        clumped_out.write(str(num_nodes) + "\t" + str(run) + "\t" + str(clumped_obj) + "\t" + str(clumped_obj_dd))
        for metric in clumped_metrics.to_list():
            clumped_out.write("\t" + str(metric))
        clumped_out.write("\n")

        bounce_out.write(str(num_nodes) + "\t" + str(run) + "\t" + str(bounce_obj) + "\t" + str(bounce_obj_dd))
        for metric in bounce_metrics.to_list():
            bounce_out.write("\t" + str(metric))
        bounce_out.write("\n")

        power_out.write(str(num_nodes) + "\t" + str(run) + "\t" + str(power_obj) + "\t" + str(power_obj_dd))
        for metric in power_metrics.to_list():
            power_out.write("\t" + str(metric))
        power_out.write("\n")
    print cases, "cases where approx > opt, for", num_nodes, "nodes"
    print diff
    #raw_input()

opt_out.close()
ord_out.close()
rand_out.close()
clumped_out.close()
bounce_out.close()
power_out.close()
