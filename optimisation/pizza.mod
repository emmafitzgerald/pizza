# Optimisation model for user partitioning based on angular channel characteristics 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set nodes;
set groups;
set pilots;

param pi = 4.0 * atan(1.0);
param num_pilots = card{pilots};
param dominant_direction{nodes};
param spectral_spread{nodes} default 0;

var assignment{nodes, groups} binary;	    # u_kg
var order{nodes, groups, pilots} binary;    # Y_kg^i
var angle{groups, pilots};		    # t_g^p
var shift{groups, pilots};		    # s_g^p
var max_angle{groups};			    # T_g
var max_shift{groups};			    # S_g
var aux_v{groups, pilots};		    # s_g^p
var aux_z{groups, pilots};		    # z_g^p
var diff{groups, pilots} >= 0;		    # d_g^p
var selection{groups, pilots} binary;	    # m_g^p
var B >= 0;				    # prettiness


maximize prettiness:
    B;

subject to partition{k in nodes}:
    sum{g in groups} assignment[k, g] = 1;

subject to max_group_size{g in groups}:
    sum{k in nodes} assignment[k, g] <= num_pilots;

subject to ordering{k in nodes, g in groups}:
    sum{p in pilots} order[k, g, p] = assignment[k, g];

subject to unique_pilots{p in pilots, g in groups}:
    sum{k in nodes} order[k, g, p] <= 1;

subject to angle_selection{p in pilots, g in groups}:
    angle[g, p] = sum{k in nodes} dominant_direction[k] * order[k, g, p];

subject to angle_ordering{p in pilots, g in groups, q in pilots: q > p}:
    angle[g, p] <= angle[g, q] + 2.0 * pi * (1 - sum{k in nodes} order[k, g, p]);

subject to no_holes{g in groups, p in pilots: p <> num_pilots}:
    sum{k in nodes} order[k, g, p] <= sum{k in nodes} order[k, g, p+1];

subject to shift_selection{p in pilots, g in groups}:
    shift[g, p] <= pi * sum{k in nodes} spectral_spread[k] * order[k, g, p];

subject to max_angle_lower_bound{p in pilots, g in groups}:
    max_angle[g] >= angle[g, p];

subject to max_angle_upper_bound{g in groups}:
    max_angle[g] <= sum{p in pilots} aux_v[g, p];

subject to max_shift_selection{g in groups}:
    max_shift[g] = sum{p in pilots} aux_z[g, p];

subject to select_max_angle{g in groups}:
    sum{p in pilots} selection[g, p] = 1;

subject to angle_differences{g in groups, p in pilots: p <> num_pilots}:
    diff[g, p] <= angle[g, p+1] - angle[g, p] + shift[g, p+1] + shift[g, p];

subject to close_the_circle{g in groups}:
    diff[g, num_pilots] <= angle[g, 1] + 2.0 * pi - max_angle[g] + shift[g, 1] + max_shift[g];

subject to angle_difference_bound{p in pilots, g in groups}:
    diff[g, p] <= pi;

subject to minimum_angle{g in groups, p in pilots: p <> num_pilots}:
    B <= diff[g, p] + pi * (1 - sum{k in nodes} order[k, g, p]);

subject to minimum_angle_last{g in groups}:
    B <= diff[g, num_pilots];

subject to aux_v_1{p in pilots, g in groups}:
    aux_v[g, p] <= angle[g, p];

subject to aux_v_2{p in pilots, g in groups}:
    aux_v[g, p] <= 2.0 * pi * selection[g, p];

subject to aux_v_3{p in pilots, g in groups}:
    aux_v[g, p] >= angle[g, p] + 2.0 * pi * (selection[g, p] - 1);

subject to aux_z_1{p in pilots, g in groups}:
    aux_z[g, p] <= shift[g, p];

subject to aux_z_2{p in pilots, g in groups}:
    aux_z[g, p] <= pi * selection[g, p];

subject to aux_z_3{p in pilots, g in groups}:
    aux_z[g, p] >= shift[g, p] + pi * (selection[g, p] - 1);
