#!/usr/bin/python
#
# Main program to generate scenarios and run the optimisation
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import random
import math
import cairo
import os
from shutil import copyfile
import sys
import subprocess
import pickle


# Colours for drawing nodes according to which group they are placed in
group_colours = {-1 : (0.0, 0.0, 0.0, 1.0),
        0 : (0.2, 0.2, 1.0, 1.0),
        1 : (1.0, 0.2, 0.2, 1.0),
        2 : (1.0, 0.2, 1.0, 1.0),
        3 : (0.2, 1.0, 0.2, 1.0),
        4 : (0.2, 1.0, 1.0, 1.0),
        5 : (1.0, 1.0, 0.2, 1.0)}

# Colours for drawing angular shift ranges
shift_colours = {-1 : (0.0, 0.0, 0.0, 0.5),
        0 : (0.2, 0.2, 1.0, 0.5),
        1 : (1.0, 0.2, 0.2, 0.5),
        2 : (1.0, 0.2, 1.0, 0.5),
        3 : (0.2, 1.0, 0.2, 0.5),
        4 : (0.2, 1.0, 1.0, 0.5),
        5 : (1.0, 1.0, 0.2, 0.5)}

# Class for user nodes
class Node:
    # Each node needs a unique ID for AMPL
    ID_counter = 0 

    def __init__(self, angle=-1, spread=-1, ID=-1):
        # If dominant direction (angle) and/or spectral spread are not provided,
        # generated them randomly
        if angle == -1:
            self.angle = random.uniform(0.0, 2.0 * math.pi)
        else:
            self.angle = angle

        if spread == -1:
            self.spread = random.uniform(0.0, 0.15)
        else:
            self.spread = spread

        if ID == -1:
            Node.ID_counter += 1
            self.ID = Node.ID_counter
        else:
            self.ID = ID

        self.group = -1

    # Assign this node to the specified group
    def assign_group(self, group):
        self.group = group

    def get_group(self):
        return group

    def get_colour(self):
        return group_colours[self.group]

    def get_shifted_colour(self):
        return shift_colours[self.group]

    def get_angle(self):
        return self.angle

    def get_spread(self):
        return self.spread

    def get_shifted_angles(self):
        # Calculate maximum angular shifts, mod 2*pi
        smaller = self.angle - (self.spread * math.pi)
        if smaller < 0:
            smaller += 2.0 * math.pi

        bigger = self.angle + (self.spread * math.pi)
        if bigger > 2.0 * math.pi:
            bigger -= 2.0 * math.pi

        return (smaller, bigger)

    def get_ID(self):
        return self.ID
    
    @staticmethod
    def reset_ID():
        Node.ID_counter = 0

# Generate a scenario with a number of nodes around a circle
def generate_scenario(num_nodes=20, randomise=True):
    nodes = []
    for i in range(num_nodes):
        if randomise:
            node = Node()
        else:
            node = Node(spread=0.0, angle=0)
        nodes.append(node)

    return nodes

# Assign random groups (for testing)
def random_groups(nodes, num_groups=3):
    for node in nodes:
        node.assign_group(random.choice(xrange(num_groups)))

# Calculate position on a circle from angle
def angle_to_pos(angle, centre_x=0.5, centre_y=0.5, radius=0.45):
    x = radius * math.cos(angle) + centre_x
    y = radius * math.sin(angle) + centre_y
    return x, y

# Draw a circle with Cairo
def draw_circle(context, centre_x, centre_y, radius, colour=(0.0, 0.0, 0.0, 1.0), fill=False):
    context.save()
    context.translate(centre_x, centre_y)
    context.scale(radius, radius)
    context.arc(0.0, 0.0, 1.0, 0.0, 2.0 * math.pi)
    context.set_source_rgba(*colour)
    if fill:
        context.fill()
    else:
        context.set_line_width(0.02)
        context.stroke()
    context.restore()

# Create an image of the scenario, with coloured node groups if they have been assigned,
# and write to a PDF file
# Only works for generated scenarios
def draw_scenario(nodes, width=200, height=200, filename="scenario.pdf", draw_shifts=True):
    surface = cairo.PDFSurface(filename, width, height)
    context = cairo.Context(surface)
    context.scale(width, height)
    
    # Draw the circle where the nodes sit
    draw_circle(context, 0.5, 0.5, 0.45)

    # Draw a circle to show the base station position
    draw_circle(context, 0.5, 0.5, 0.01, fill=True)

    for node in nodes:
        x, y = angle_to_pos(node.get_angle())
        colour = node.get_colour()
        draw_circle(context, x, y, 0.015, colour, fill=True)
        if draw_shifts:
            smaller, bigger = node.get_shifted_angles()
            x, y = angle_to_pos(smaller)
            colour = node.get_shifted_colour()
            draw_circle(context, x, y, 0.01, colour, fill=True)
            x, y = angle_to_pos(bigger)
            draw_circle(context, x, y, 0.01, colour, fill=True)

# Output a scenario as AMPL data
def output_scenario(nodes, num_pilots, num_groups, data_path="scenario.dat", pizza_path="scenario_channels.dat", include_angles=True):
    data_file = open(data_path, "w")
    
    data_file.write("data;\n\n")

    data_file.write("set groups := ")
    for i in range(num_groups):
        data_file.write(str(i+1) + " ")
    data_file.write(";\n\n")

    data_file.write("set pilots := ")
    for i in range(num_pilots):
        data_file.write(str(i+1) + " ")
    data_file.write(";\n\n")
    
    data_file.write("set nodes := ")
    for node in nodes:
        data_file.write(str(node.ID) + " ")
    data_file.write(";\n\n")

    if include_angles:
        pizza_file = open(pizza_path, "w")
        pizza_file.write("data;\n\n")

        pizza_file.write("param: dominant_direction :=\n")
        for node in nodes:
            pizza_file.write(str(node.ID) + "\t" + str(node.angle) + "\n")
        pizza_file.write(";\n\n")

        pizza_file.write("param: spectral_spread :=\n")
        for node in nodes:
            pizza_file.write(str(node.ID) + "\t" + str(node.spread) + "\n")
        pizza_file.write(";\n\n")
        
        pizza_file.close()

    data_file.close()

# Clean up the directory. Deletes all scenarios, solutions, and generated images.
def clean_directory(directory="."):
    os.chdir(directory)
    for directory in ["scenario_data", "scenario_images", "solution_images", "solutions"]:
        os.chdir(directory)
        os.system("rm -f ./*")
        os.chdir("..")
    os.chdir("..")

# Run the optimisation using AMPL and return the running time
def run_ampl(nodes, output_path="solution.out", run_file="pizza.run"):
    p = subprocess.Popen(["time", "-f %U %S", "ampl", run_file],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outfile = open(output_path, "w") 
    for line in p.stdout:
        outfile.write(line.decode("ascii"))
    outfile.close()
    ttime = -1.0 
    for line in p.stderr:
        pieces = line.split()
        try: 
            ttime = float(pieces[0]) + float(pieces[1])
        except:
            print(run_file)
            print(output_path)
            print(line) 
            continue
    print("Solution time:", ttime)

    return ttime

# Parse an AMPL solution file and write data as a list of Nodes with groups assigned
def parse_solution(nodes, solution_path="solution.out"):
    solution_file = open(solution_path, "r")
    objective = -1

    state = "START"
    for line in solution_file:
        if "objective" in line:
            objective = float(line.split()[-1])
            continue
        if line.startswith(";"):
            state = "START"
            continue
        if line.startswith("assignment"):
            state = "GROUP_ASSIGNMENT"
            continue
        if state == "GROUP_ASSIGNMENT":
            pieces = line.split()
            node_id = int(pieces[0])
            group_id = int(pieces[1]) - 1
            nodes[node_id-1].assign_group(group_id)
            continue

    solution_file.close()

    return objective

if __name__ == "__main__":
    # Uncomment to clean up the directory before running.
    #clean_directory()
    
    num_runs = 20
    max_num_pilots = 12

    # Set to False to instead use already-generated data, for example from the S-V model
    generate_scenarios = True

    for num_nodes in [15, 18, 21, 24, 27, 30, 33, 36]:
        print("\n\nNumber of nodes: " + str(num_nodes))
        for run in range(num_runs):
            i = run + 1
            print("Run number:", i)
            sys.stdout.flush()

            config = str(num_nodes) + "nodes_run" + str(i) + "_"

            Node.reset_ID()

            if generate_scenarios:
                nodes = generate_scenario(num_nodes=num_nodes)
            else:
                nodes = generate_scenario(num_nodes=num_nodes, randomise=False)

            # Uncomment (and comment following line) to automatically calculate the 
            # number of groups to use based on the number of available pilots
            #num_groups = int(math.ceil(num_nodes / float(max_num_pilots)))
            num_groups = 3

            print("Number of nodes:", num_nodes)
            print("Max number of pilots:", max_num_pilots)
            print("Number of groups:", num_groups)
            num_pilots = int(num_nodes / num_groups)
            # fix integer division
            if num_pilots * num_groups < num_nodes:
                num_pilots += 1
            print("Number of pilots for each group:", num_pilots)
            sys.stdout.flush()

            data_path = "scenario_data/scenario_" + config + ".dat"
            pizza_data_path = "scenario_data/scenario_" + config + "_pizza.dat"
            print("Writing scenario data...")
            sys.stdout.flush()

            if generate_scenarios:
                output_scenario(nodes, num_pilots, num_groups, data_path=data_path, pizza_path=pizza_data_path,  include_angles=True)
            else:
                output_scenario(nodes, num_pilots, num_groups, data_path=data_path, include_angles=False)

            image_path = "scenario_images/scenario_" + config + ".pdf"
            print("Drawing scenario...")
            sys.stdout.flush()
            draw_scenario(nodes, filename=image_path)

            copyfile(data_path, "pizza.dat")
            copyfile(channel_data_path, "channels.dat")
            solution_path = "solutions/solution_" + config + ".out"
            print("Solving with AMPL...")
            sys.stdout.flush()
            solution_time = run_ampl(nodes, output_path=solution_path, run_file="pizza.run")
            
            objective = parse_solution(nodes, solution_path=solution_path)
            print("Objective:", objective)
            sys.stdout.flush()
            image_path = "solution_images/scenario_" + config + ".pdf"
            print("Drawing solution...")
            sys.stdout.flush()
            draw_scenario(nodes, filename=image_path)

            print()

    print("Done.")
