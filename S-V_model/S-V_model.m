% Modified Saleh-Valenzuela channel model 
%
% (C) Copyright 2019 Harsh Tataria, Emma Fitzgerald
% harsh.tataria@eit.lth.se, emma.fitzgerald@eit.lth.se
%
% This program is distributed under the terms of the GNU Lesser General Public License.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

clc;
clear all;
close all;
rng('shuffle')
warning('off','all');

%% System Parameters at 28 GHz
f = 2470e5;       % Frequency
speedoflight = 299792458;  % Speed of light
lambda = speedoflight/f;   % Wavelength
antennaLength = 1/2;
phi_std_mean = (pi/180)*7.5;
theta_std_mean = phi_std_mean;

Nsim = 20;
M = 100; % # TX antennas
NtRF = 4;

num_users = [15, 18, 21, 24, 27, 30, 33, 36];
P = 1;  % # RX antennas per user

L = 10;
d_lambda = 1/2;

for i = 1:size(num_users, 2),
    K = num_users(i);
    for nsim = 1:Nsim,
	nsim

	%% Generate clusters in the propagation channel
	C = 4;
	gamma = 1/C.*ones(C,1);
	
	dominant_directions = zeros(1,K);
	spectral_spreads = zeros(1,K);
	H_URA = zeros(K*P,M);
	for k = 1:K,
	    %% Angle generation and steering vectors
	    g_URA = zeros(P,M,C);
	    At_URA = zeros(M,C*L);
	    At_URA_q2 = zeros(M,C*L);
	    At_URA_q4 = zeros(M,C*L);
	    central_angles = zeros(1,C);
	    cl = 0;
	    biggest_angle = 0;
	    smallest_angle = 7;
	    for c = 1:C,
		% Central angles
		theta_0 = deg2rad(unifrnd(45,135));
		if theta_0 > biggest_angle,
		    biggest_angle = theta_0;
		end
		if theta_0 < smallest_angle,
		    smallest_angle = theta_0;
		end
		central_angles(c) = theta_0;
		phi_0 = deg2rad(unifrnd(120,240));
		% Angle Standard Deviation (spread of angles)
		sigma_phi = exprnd(phi_std_mean);
		sigma_theta = exprnd(theta_std_mean);
		% Perterbations
		coin = rand(L,1)-0.5;
		b_phi = sigma_phi/sqrt(2);
		b_theta = sigma_theta/sqrt(2);
		Delta_phi = phi_0-(b_phi*sign(coin).*log(1-(2*abs(coin))));
		Delta_theta = theta_0-(b_theta*sign(coin).*log(1-(2*abs(coin))));
		
		%% Antenna Steering Vectors
		% URA
		at_dxvec_URA = d_lambda*repmat(0:sqrt(M)-1,[1 sqrt(M)]).';
		at_dyvec_URA = d_lambda*zeros(M,1);
		at_dzvec_URA = d_lambda*floor((0:(M-1))/sqrt(M)).';
		%
		ar_dxvec_URA = d_lambda*repmat(0:sqrt(P)-1,[1 sqrt(P)]).';
		ar_dyvec_URA = d_lambda*zeros(P,1);
		ar_dzvec_URA = d_lambda*floor((0:(P-1))/sqrt(P)).';
		%
		%% Small Scale Fading
		gcl_URA = zeros(P,M,L);
		for l = 1:L,
		    cl = cl+1;
		    % URA
		    at_URA = exp(1j*2*pi*at_dxvec_URA*sin(Delta_theta(l))*cos(Delta_phi(l))).*exp(1j*2*pi*at_dyvec_URA*sin(Delta_theta(l))*sin(Delta_phi(l))).*exp(1j*2*pi*at_dzvec_URA*cos(Delta_theta(l)));
		    
		    ar_URA = exp(1j*2*pi*ar_dxvec_URA*sin(Delta_theta(l))*cos(Delta_phi(l))).*exp(1j*2*pi*ar_dyvec_URA*sin(Delta_theta(l))*sin(Delta_phi(l))).*exp(1j*2*pi*ar_dzvec_URA*cos(Delta_theta(l)));
		    
		    at_URA = at_URA./at_URA(1); % Normalize
		    
		    ar_URA = ar_URA./ar_URA(1);
		    
		    At_URA(:,cl) = at_URA;
		    
		    gcl_URA(:,:,l) = sqrt(cluster_scale)*(1/sqrt(M))*(1/sqrt(P))*(1/sqrt(L))*(1/sqrt(2))*(normrnd(0,sigma) + 1j*normrnd(0,sigma))*ar_URA*at_URA';
		end
		g_URA(:,:,c) = sum(gcl_URA,3);
	    end

	    % Sum over all base station and user antennas to get a total amplitude for each component
	    comp_ampl = sum(sum(abs(g_URA).^2,1),2);
	    old_total_ampl = sum(comp_ampl);
	    % Select the component with the largest total amplitude as the dominant one
	    [dom_comp_ampl, dom_comp_idx] = max(comp_ampl);
	    % LOS component with higher amplitude
	    g_URA(:,:,dom_comp_idx) = (2 + rand()*4)*g_URA(:,:,dom_comp_idx); 
	    % Renormalise
	    comp_ampl = sum(sum(abs(g_URA).^2,1),2);
	    new_total_ampl = sum(comp_ampl);
	    scale = sqrt(old_total_ampl / new_total_ampl);
	    g_URA(:,:,:) = scale*g_URA(:,:,:); 
	    % Recalculate amplitude of dominant component
	    comp_ampl = sum(sum(abs(g_URA).^2,1),2);
	    [dom_comp_ampl, dom_comp_idx] = max(comp_ampl);
	    % Record the angle of the dominant component
	    dom_direction = central_angles(dom_comp_idx);
	    % Calculate the Rician K factor to use as the spectral spread
	    non_dom_ampl = sum(comp_ampl);
	    ss_scale = deg2rad(360.0) / (biggest_angle - smallest_angle);
	    rician_K_factor_spread = (1.0 - (dom_comp_ampl / non_dom_ampl))/ss_scale;

	    % Circular mean
	    total_power = sum(comp_ampl) * sum(comp_ampl);
	    sin_sum = 0;
	    cos_sum = 0;
	    for c = 1:C
		angle = central_angles(c)
		power = (comp_ampl(c) * comp_ampl(c)) / total_power;
		sin_sum = sin_sum + power * sin(angle);
		cos_sum = cos_sum + power * cos(angle);
	    end

	    dominant_directions(k) = dom_direction;
	    spectral_spreads(k) = rician_K_factor_spread;

	    clear tmp_H_URA
	    tmp_H_URA = sum(g_URA,3);
	    co = 1;
	    for i = ((P*(k-1))+1):((P*(k-1))+P),
		H_URA(i,:) = tmp_H_URA(co,:);
		co = co+1;
	    end
	end

	% Write scenarios to file in AMPL data format
	users = [1:K];
	data = [users; dominant_directions];
	path = ['scenarios/' num2str(K) '_nodes_' num2str(nsim) '_pizza_data.dat'];
	fileID = fopen(path, 'w');
	fprintf(fileID, 'data;\n\n');
	fprintf(fileID, 'param: dominant_direction :=\n');
	fprintf(fileID, '%d\t%f\n', data);
	fprintf(fileID, ';\n\n');
	data = [users; spectral_spreads];
	fprintf(fileID, 'param: spectral_spread :=\n');
	fprintf(fileID, '%d\t%f\n', data);
	fprintf(fileID, ';\n');
	fclose(fileID);

	path = ['scenarios/' num2str(K) '_nodes_' num2str(nsim) '_channel_data.dat'];
	fileID = fopen(path, 'w');
	for k = 1:K,
	    for m = 1:M,
		fprintf(fileID, '%f%+fj\t', real(H_URA(k, m)), imag(H_URA(k, m)));
	    end
	    fprintf(fileID, '\n');
	end
	fclose(fileID);
    end
end
