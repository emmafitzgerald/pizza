#!/usr/bin/python

import math
import numpy as np
import scipy.stats
import matplotlib
matplotlib.use("Agg")
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt 
import os

colours = ["blue", "red", "yellow", "purple", "orange", "green", "black", "cyan"]

def parse_solution_times():
    log = open("scenario.log", "r")
    data = open("solution_times.dat", "w")
    data.write("# Num nodes\tRun\tSolution time\tObjective\n")

    state = "START"
    for line in log:
        if state == "START":
            if "Run number:" in line:
                run_num = int(line.split()[-1])
                state = "NUM_NODES"
            continue

        if state == "NUM_NODES":
            if "Number of nodes:" in line:
                num_nodes = int(line.split()[-1])
                state = "TIME"
            continue

        if state == "TIME":
            if "Solution time:" in line:
                time = float(line.split()[-1])
                state = "OBJ"
            continue

        if state == "OBJ":
            if "Objective:" in line:
                obj = float(line.split()[-1])
                state = "START"

                data.write(str(num_nodes) + "\t" + str(run_num) + "\t" + str(time) + "\t" + str(obj) + "\n")
            continue

    log.close()
    data.close()

# Get average and confidence interval delta of a vector of values
def process_vector(v):
    if len(v) == 0:
        print "Empty vector"
        return (0, 0)  
    avg = np.mean(v)
    stddev = np.std(v)
    n = len(v)
    delta = 1.96 * (stddev / math.sqrt(n))
    return (avg, delta)

def bar_solution_times(data, filename):
    fig, ax = plt.subplots()

    num_nodes = [15, 18, 21, 24, 27, 30, 33, 36]
    vals = []
    err = []

    for x in num_nodes:
        idx = data[:,0] == x
        sel_data = data[idx]
        times = sel_data[:,2]
        avg, delta = process_vector(times)
        vals.append(avg)
        err.append(delta)

    plt.bar(num_nodes, vals, yerr=err)
    plt.xlabel("Number of nodes")
    plt.ylabel("Solution time (s)")
    plt.yscale("log")
    plt.xticks(num_nodes)

    plt.savefig(filename)
    plt.cla()

def scatter_time_obj(data, filename):
    fig, ax = plt.subplots()

    i = 0
    for num_nodes in [15, 18, 21, 24, 27, 30, 33, 36]:
        idx = data[:,0] == num_nodes
        sel_data = data[idx]
        time = sel_data[:,2]
        obj = sel_data[:,3]

        plt.scatter(obj, time, color=colours[i])
        i += 1

    plt.xlabel("Objective")
    plt.ylabel("Solution time (s)")
    plt.yscale("log")

    plt.savefig(filename)
    plt.cla()

#parse_solution_times()
data = np.genfromtxt("solution_times.dat")
bar_solution_times(data, "figures/solution_times_vs_num_nodes.pdf")
scatter_time_obj(data, "figures/scatter_time_obj.pdf")
